<%@include file="header.jsp" %>

<body>
    <div class="container">
        <h1><c:out value="${titulo}"/></h1>
        <hr>
        <form:form cssClass="form-horizontal">
            <div class="form-group">
                <form:label class="control-label col-sm-2" path="nombreUsuarioAd">Usuario: </form:label>
                    <div class="col-sm-10">
                    <form:input class="form-control" path="nombreUsuarioAd"></form:input>
                    </div>
                </div>
                <div class="form-group">
                <form:label class="control-label col-sm-2" path="passwordAdministrador">Password: </form:label>
                    <div class="col-sm-10">
                    <form:input class="form-control" path="passwordAdministrador"></form:input>
                    </div>
                </div>
                <div class="form-group">
                <form:label class="control-label col-sm-2" path="nombrePersonaAd">Nombre: </form:label>
                    <div class="col-sm-10">
                    <form:input class="form-control" path="nombrePersonaAd"></form:input>
                    </div>
                </div>
                <div class="form-group">
                <form:label class="control-label col-sm-2" path="apellidoPersonaAd">Apellido: </form:label>
                    <div class="col-sm-10">
                    <form:input class="form-control" path="apellidoPersonaAd"></form:input>
                    </div>
                </div>
                <div class="form-group">
                <form:label class="control-label col-sm-2" path="telefonoAdministrador">Telefono: </form:label>
                    <div class="col-sm-10">
                    <form:input class="form-control" path="telefonoAdministrador"></form:input>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                    <form:button class="btn btn-success" ><span class="glyphicon glyphicon-refresh"></span> Actualizar</form:button> <a class="btn btn-danger" role="button" href="administradorList.htm"><span class="glyphicon glyphicon-remove-sign"></span> Cancelar</a>
                    </div>
                </div>
        </form:form>
    </div>
</body>
</html>