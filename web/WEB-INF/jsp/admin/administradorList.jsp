
<%@include file="header.jsp" %>

<body>

    <div class="container">
        <h1>${titulo}</h1>
        <hr>
        <a class="btn btn-success" role="button" href="administradorAdd.htm">Adicionar Administrador</a> <a class="btn btn-default" role="button" href="administradorList.htm">Regresar</a>
        <hr>
        <div class="table-responsive">
            <table class="table table-hover">
                <tr>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Password</th>
                    <th>Nombre</th>
                    <th>Apellido</th>
                    <th>Telefono</th>
                    <th>Accion</th>
                </tr>
                <c:forEach items="${listaAdministradores}" var="administrador">
                    <tr>
                        <td><c:out value="${administrador.idAdministrador}"/></td>
                        <td>${administrador.nombreUsuarioAd}</td>
                        <td>${administrador.passwordAdministrador}</td>
                        <td>${administrador.nombrePersonaAd}</td>
                        <td>${administrador.apellidoPersonaAd}</td>
                        <td>${administrador.telefonoAdministrador}</td>
                        <td><a class="btn btn-warning" role="button" href="administradorEdit.htm?id=${administrador.idAdministrador}"><span class="glyphicon glyphicon-pencil"></span></a> <a class="btn btn-danger" role="button" href="administradorDelete.htm?id=${administrador.idAdministrador}"><span class="glyphicon glyphicon-trash"></span></a></td>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>

</body>
</html>
