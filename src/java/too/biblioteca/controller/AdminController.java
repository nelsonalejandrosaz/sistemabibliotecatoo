/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package too.biblioteca.controller;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import too.biblioteca.models.Administrador;

/**
 *
 * @author nelso
 */
@Controller
public class AdminController {

    AdministradorHibernateController administradorHC = new AdministradorHibernateController();

    @RequestMapping("/administradorList.htm")
    public ModelAndView administradorLista() {
        ModelAndView mav = new ModelAndView("admin/administradorList");
        String titulo = "Lista Administradores";
        List<Administrador> administradors = administradorHC.getAllAdministrador();
        mav.addObject("listaAdministradores", administradors);
        mav.addObject("titulo", titulo);
        return mav;
    }

    @RequestMapping(value = "/administradorEdit.htm",method = RequestMethod.GET)
    public ModelAndView administradorEdicion(HttpServletRequest request) {
        String titulo = "Editar Administrador";
        String idString = request.getParameter("id");
        int id = Integer.parseInt(idString);
        Administrador administrador = administradorHC.getAdministradorByID(id);
        ModelAndView mav = new ModelAndView("admin/administradorEdit","command",administrador);
        mav.addObject("titulo", titulo);
        mav.addObject("idR",idString);
        return mav;
    }
    
    @RequestMapping(value = "/administradorDelete.htm", method = RequestMethod.GET)
    public ModelAndView administradorDelete(HttpServletRequest request){
        String idString = request.getParameter("id");
        int id = Integer.parseInt(idString);
        administradorHC.deleteAdministrador(id);
        ModelAndView mav = new ModelAndView("admin/administradorDelete");
        return mav;
    }
    
    @RequestMapping(value = "/administradorEdit.htm",method = RequestMethod.POST)
    public void administradorEdicion(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Administrador administrador = new Administrador();
        int idAdministrador = Integer.parseInt(request.getParameter("id"));
        administrador.setIdAdministrador(idAdministrador);
        administrador.setNombreUsuarioAd(request.getParameter("nombreUsuarioAd"));
        administrador.setPasswordAdministrador(request.getParameter("passwordAdministrador"));
        administrador.setNombrePersonaAd(request.getParameter("nombrePersonaAd"));
        administrador.setApellidoPersonaAd(request.getParameter("apellidoPersonaAd"));
        administrador.setTelefonoAdministrador(Integer.parseInt(request.getParameter("telefonoAdministrador")));
        administradorHC.updateAdministrador(administrador);
        response.sendRedirect("administradorList.htm");
    }
    
    @RequestMapping(value = "administradorAdd.htm")
    public ModelAndView administradorAdd(){
        Administrador administrador = new Administrador();
        String titulo = "Agragar usaurio administrador";
        ModelAndView mav = new ModelAndView("admin/administradorAdd","command",administrador);
        mav.addObject("titulo",titulo);
        return mav;
    }
    
    @RequestMapping(value = "administradorAdd.htm", method = RequestMethod.POST)
    public void administradorAdd(HttpServletRequest request,HttpServletResponse response) throws IOException{
        ModelAndView mav = new ModelAndView();
        String titulo = "Adicionar Administrador";
        Administrador administrador = new Administrador();
        administrador.setNombreUsuarioAd(request.getParameter("nombreUsuarioAd"));
        administrador.setPasswordAdministrador(request.getParameter("passwordAdministrador"));
        administrador.setNombrePersonaAd(request.getParameter("nombrePersonaAd"));
        administrador.setApellidoPersonaAd(request.getParameter("apellidoPersonaAd"));
        administrador.setTelefonoAdministrador(Integer.parseInt(request.getParameter("telefonoAdministrador")));
        administradorHC.addAdministrador(administrador);
        mav.addObject("titulo", titulo);
        response.sendRedirect("administradorList.htm");
    }
    
    
}
