/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package too.biblioteca.controller;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import too.biblioteca.models.Administrador;

/**
 *
 * @author nelso
 */
public class AdministradorHibernateController {

    public void addAdministrador(Administrador administrador) {
        try {
            SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
            Session session;
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
            session.save(administrador);
            transaction.commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public List<Administrador> getAllAdministrador() {
        List<Administrador> administradorLista = new ArrayList<Administrador>();
        try {
            SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
            Session session;
            session = sessionFactory.openSession();
            Transaction transaction = session.beginTransaction();
//        Aqui va la operacion del CRUD
            administradorLista = session.createQuery("from Administrador").list();
//        Hibernate general para cerrar transaccion
//        transaction.commit();
            session.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return administradorLista;
    }

    public Administrador getAdministradorByID(int idAdministrador) {
//        Administrador administrador = new Administrador(1,"qwert", "qwerty", "Juan", "Anzora", 22222222);
        SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String queryString = "from Administrador where idAdministrador = :id ";
        Query query = session.createQuery(queryString);
        query.setInteger("id", idAdministrador);
        Administrador administrador = (Administrador) query.uniqueResult();
//        administrador = (Administrador) session.load(Administrador.class, idAdministrador);
        session.getTransaction().commit();
        session.close();
        return administrador;
    }

    public void updateAdministrador(Administrador administrador) {
        SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.update(administrador);
        transaction.commit();
        session.close();
    }
    
    public void deleteAdministrador(int idAdministrador){
        SessionFactory sessionFactory = NewHibernateUtil.getSessionFactory();
        Session session;
        session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        String queryString = "from Administrador where idAdministrador = :id ";
        Query query = session.createQuery(queryString);
        query.setInteger("id", idAdministrador);
        Administrador administrador = (Administrador) query.uniqueResult();
        session.delete(administrador);
        transaction.commit();
        session.close();
    }

}
