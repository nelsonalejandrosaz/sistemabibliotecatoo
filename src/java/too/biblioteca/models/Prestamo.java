package too.biblioteca.models;
// Generated 10-26-2016 11:03:01 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Prestamo generated by hbm2java
 */
public class Prestamo  implements java.io.Serializable {


     private int idPrestamo;
     private Lector lector;
     private Recurso recurso;
     private Date fechaPrestamo;
     private Date fechaDevolucion;
     private byte estado;

    public Prestamo() {
    }

    public Prestamo(int idPrestamo, Lector lector, Recurso recurso, Date fechaPrestamo, Date fechaDevolucion, byte estado) {
       this.idPrestamo = idPrestamo;
       this.lector = lector;
       this.recurso = recurso;
       this.fechaPrestamo = fechaPrestamo;
       this.fechaDevolucion = fechaDevolucion;
       this.estado = estado;
    }
   
    public int getIdPrestamo() {
        return this.idPrestamo;
    }
    
    public void setIdPrestamo(int idPrestamo) {
        this.idPrestamo = idPrestamo;
    }
    public Lector getLector() {
        return this.lector;
    }
    
    public void setLector(Lector lector) {
        this.lector = lector;
    }
    public Recurso getRecurso() {
        return this.recurso;
    }
    
    public void setRecurso(Recurso recurso) {
        this.recurso = recurso;
    }
    public Date getFechaPrestamo() {
        return this.fechaPrestamo;
    }
    
    public void setFechaPrestamo(Date fechaPrestamo) {
        this.fechaPrestamo = fechaPrestamo;
    }
    public Date getFechaDevolucion() {
        return this.fechaDevolucion;
    }
    
    public void setFechaDevolucion(Date fechaDevolucion) {
        this.fechaDevolucion = fechaDevolucion;
    }
    public byte getEstado() {
        return this.estado;
    }
    
    public void setEstado(byte estado) {
        this.estado = estado;
    }




}


